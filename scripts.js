var config = {
    kgtolbs: 2.2,
    offset: 20,
    pixelsPerInch: 4,
    //fuellbs: 308,
    fuellbs: 308
};

var aircrafts = {
    sppac: {
        make: "Pacific Aerospace",
        model: "PAC750XL",
        reg: "SP-PAC",
        bew: 3777.85, // Basic Empty Weight in LBS -
        rampArm: 111.67, // CoG of Empty (BEW) Aircraft - INCHES from DATUM
        fuelArm: 110.21, // Fuel Tank Arm (used for skydiving operations)
        fuellbs: 308, // Common fuel configuration with skydivers
        mtow: 7500, // or RAMP WEIGHT if used
        minArm: 102.18, // CoG Forward Limit
        maxArm: 124.60, // CoG Aft Limit
        frontLimit: 82.34, // Forward compartment limit
        rearLimit: 240.08, //  Aft compartment limit
        doorStart: 187, // Forward Door's Arm
        doorEnd: 237, // Aft Door's Arm
        class: "sppac", //registration without any special characters
        offset: 0 // DON'T CHANGE - przesuniecie calego systemu miarowania o offset (-50 to w lewo o 50px), zeby skala miarki nie zaczynala sie od 0
    },
    spwaw: {
        make: "Cessna",
        model: "C208B",
        reg: "SP-WAW",
        bew: 4736, // Basic Empty Weight in LBS
        rampArm: 182.4, // CoG of Empty (BEW) Aircraft - INCHES from DATUM
        fuelArm: 205, // Fuel Tank Arm (used for skydiving operations)
        fuellbs: 500, // Common fuel configuration with skydivers
        mtow: 8750, // or RAMP WEIGHT if used
        minArm: 179.6, // CoG Forward Limit
        maxArm: 204.35, // CoG Aft Limit
        frontLimit: 100, // Forward compartment limit
        rearLimit: 356, // Aft Door's Arm
        doorStart: 282, // Forward Door's Arm
        doorEnd: 332, // Aft Door's Arm
        class: "spwaw", //registration without any special characters
        offset: -290 // DON'T CHANGE - przesuniecie calego systemu miarowania o offset (-50 to w lewo o 50px), zeby skala miarki nie zaczynala sie od 0
    },
    dfprf: {
        make: "Cessna",
        model: "C208",
        reg: "D-FPRF",
        bew: 4600, // Basic Empty Weight in LBS
        rampArm: 166.3, // CoG of Empty (BEW) Aircraft - INCHES from DATUM
        fuelArm: 185, // Fuel Tank Arm (used for skydiving operations)
        fuellbs: 500, // Common fuel configuration with skydivers
        mtow: 8359, // or RAMP WEIGHT if used
        minArm: 163.5, // CoG Forward Limit
        maxArm: 184.1, // CoG Aft Limit
        frontLimit: 100, // Forward compartment limit
        rearLimit: 308, // Aft Door's Arm
        doorStart: 234, // Forward Door's Arm
        doorEnd: 284, // Aft Door's Arm
        class: "dfprf", //registration without any special characters
        offset: -290 // DON'T CHANGE - przesuniecie calego systemu miarowania o offset (-50 to w lewo o 50px), zeby skala miarki nie zaczynala sie od 0
    },
    pink: {
        make: "Shorts (Pilot [185 lbs] included)",
        model: "SC7 Skyvan",
        reg: "PINK",
        bew: 8231, // Basic Empty Weight in LBS
        rampArm: 169.97, // CoG of Empty (BEW) Aircraft - INCHES from DATUM
        fuelArm: 154.52, // Fuel Tank Arm (used for skydiving operations)
        fuellbs: 1100, // Common fuel configuration with skydivers
        mtow: 12500, // or RAMP WEIGHT if used
        minArm: 162, // CoG Forward Limit
        maxArm: 177, // CoG Aft Limit
        frontLimit: 74, // Forward compartment limit
        rearLimit: 297.5, // Aft Door's Arm
        doorStart: 132.8, // Forward Door's Arm
        doorEnd: 297.5, // Aft Door's Arm
        class: "pink", //registration without any special characters
        offset: 0 // DON'T CHANGE - przesuniecie calego systemu miarowania o offset (-50 to w lewo o 50px), zeby skala miarki nie zaczynala sie od 0
    }
}

let searchParams = new URLSearchParams(window.location.search);
var aircraft = aircrafts.sppac;
if (searchParams.has('reg')) {
    let param = searchParams.get('reg');
    console.log(param)
    aircraft = aircrafts[param];
    console.log(aircraft)
    $('#canvas').addClass(aircraft.class);
}



function drawResult() {

    var mtow = aircraft.mtow;
    var bew = aircraft.bew;
    var fuelLBS = $("#aircraftFuelWeightLbs").innerText;
    var fuelMomentum = fuelLBS * aircraft.fuelArm / 1000;

    var paylodAvail = mtow - bew - fuelLBS;

    // Ramp momentum /1000
    var rampmomentum = (aircraft.bew * aircraft.rampArm / 1000);
    var rampmomentumwithFuel = rampmomentum + fuelMomentum;

    // Ramp CoG (total momentum / total mass) in inches aft of datum
    var weightwithFuel = aircraft.bew + fuelLBS;

    var cog  = rampmomentumwithFuel / weightwithFuel;

    var offset = config.offset + aircraft.offset;
    // half of the width (after 90deg rotation - height actually) of .skydiver element

    var pixelsPerInch = config.pixelsPerInch;
    // how many pixels for 1 inch of aircraft view

    var weight = 0;

    var left = 0;

    var arm = left / pixelsPerInch;
    var momentum = arm  * weight * config.kgtolbs / 1000;

    var payload = 0;
    var payloadArm = 0;
    var sumamomentum2 = 0;
    $('#canvas').children('.skydiver').each(function() {

        var arm2 = $(this).position().left / pixelsPerInch;


        if ( $(this).hasClass('boarded') ) {
            var weight2 = $(this).attr("data-weightkg");
            var weightbackup = weight2;
            var momentum2 = arm2 * weight2 * config.kgtolbs / 1000;
        } else {
            var weightbackup = $(this).attr("data-weightkg");
            weight2 = 0;
            momentum2 = 0;
            $(this).removeClass('boarded');
        }
        $(this).text(weightbackup + ' kg');

        payloadArm = payloadArm + arm2;
        sumamomentum2 = sumamomentum2 + momentum2;
        payload = payload + parseInt(weight2);

    } );

    var payloadlbs = payload * config.kgtolbs;
    var rampweight = payloadlbs + bew + fuelLBS;

    var rampmomentum = sumamomentum2 + rampmomentum + fuelMomentum;
    var ramparm = rampmomentum * 1000 / rampweight;

    var weightLbs = weight * config.kgtolbs;

    $("#bew").text(aircraft.bew.toFixed(2));
    $("#bewArm").text(aircraft.rampArm.toFixed(2));
    var bewMomentum = aircraft.bew * aircraft.rampArm / 1000;
    $("#bewMomentum").text(bewMomentum.toFixed(2));

    $("#payload").text(payloadlbs.toFixed(2));
    $("#payloadArm").text((sumamomentum2/payloadlbs).toFixed(2));
    $("#payloadMomentum").text(sumamomentum2.toFixed(2));

    var weightLessFuel = aircraft.bew + payloadlbs;
    var weightLessFuelArm = aircraft.rampArm + payloadArm / 1000;
    var weightLessFuelMomomentum = weightLessFuel * weightLessFuelArm / 1000;
    $("#weightLessFuel").text(weightLessFuel.toFixed(2));
    $("#weightLessFuelArm").text(weightLessFuelArm.toFixed(2));
    $("#weightLessFuelMomentum").text(weightLessFuelMomomentum.toFixed(2));

    $("#fuelWeight").text(aircraft.fuellbs.toFixed(2));
    $("#fuelArm").text(aircraft.fuelArm.toFixed(2));
    var fuelMomentum = aircraft.fuellbs * aircraft.fuelArm / 1000;
    $("#fuelMomentum").text(fuelMomentum.toFixed(2));

    $("#rampWeight").text(rampweight.toFixed(2));
    $("#rampArm").text(ramparm.toFixed(2));
    $("#rampMomentum").text(rampmomentum.toFixed(2));

/*    $('#results').append(
        '<br><br>____ AIRCRAFT ____' +
        '<br>Model: ' + aircraft.model +
        '<br>Reg: ' + aircraft.reg +
        '<br>Basic Empty Weight: ' + bew.toFixed(2) +
        '<br>Fuel Weight: ' + fuelLBS.toFixed(2) +
        '<br>Payload (skydivers + pilot): ' + payloadlbs.toFixed(2) +
        '<br>Payload Available: ' + paylodAvail.toFixed(2) +
        '<br>________________' +
        '<br>Total Weight: ' + rampweight.toFixed(2) +
        '<br>Total Arm: ' + ramparm.toFixed(2) +
        '<br>Payload Momentum: ' + sumamomentum2.toFixed(2) +
        '<br>Total Momentum: ' + rampmomentum.toFixed(2) +
        '<br><br>____ LIMITS ____' +
        '<br>MTOW (Max Weight): ' + mtow.toFixed(2) +
        '<br>Forward CoG limit: ' + aircraft.minArm +
        '<br>At CoG limit: ' + aircraft.maxArm +
        '<br><br>____ ACTIVE SKYDIVER ____' +
        '<br>Weight: ' + weightLbs.toFixed(2) +
       '<br>Left: </vr>' + left +
       '<br>Offset: ' + offset +
        '<br>Arm: ' + arm.toFixed(2) +
        '<br>Moment: ' + momentum.toFixed(2));*/

    var leftOffsetAfter = ramparm * config.pixelsPerInch + aircraft.offset;
    $('.vl').css( {"left": leftOffsetAfter});
}

window.onload=function() {

    $('#regAircraftTitle').html('<h1>' + aircraft.reg.toUpperCase() + '</h1><h2>' + aircraft.model.toUpperCase() + '</h2><h3>' + aircraft.make.toUpperCase() + '</h3>' );

    $("#btn2").click(function(){
        $("#canvas").toggleClass("active");
    });

    drawRuler();
    drawResult();

    function drawRuler() {


        var canvas = document.getElementById('timeline');
        var context = canvas.getContext('2d');
        var spacing = config.pixelsPerInch * 10;
        context.lineWidth = 0.1;
        context.strokeStyle = '#555';
        context.beginPath();
        for (var interval = 0; interval < 26; interval++) {
            context.moveTo(interval * spacing + 0.5, 50);
            context.lineTo(interval * spacing + 0.5, 42);
            context.stroke();
        }
    }

    var leftOffsetStart = aircraft.frontLimit * config.pixelsPerInch + aircraft.offset;
    $('#vlstart').css( {"left": leftOffsetStart, "height" : "60px", "color" : "black", "top" : "168px"});

    var leftOffsetEnd = aircraft.rearLimit * config.pixelsPerInch + aircraft.offset;
    $('#vlend').css( {"left": leftOffsetEnd, "height" : "60px", "color" : "black", "top" : "168px"});

    var leftOffsetDoorStart = aircraft.doorStart * config.pixelsPerInch + aircraft.offset;
    $('#vldoorstart').css( {"left": leftOffsetDoorStart, "height" : "30px", "color" : "black", "top" : "178px"});

    var leftOffsetDoorEnd = aircraft.doorEnd * config.pixelsPerInch + aircraft.offset;
    $('#vldoorend').css( {"left": leftOffsetDoorEnd, "height" : "30px", "color" : "black", "top" : "178px"});

}

var coordinates = function(element) {
    element = $(element);

    var weight = element.attr("data-weightkg");
    var id = element.attr('id');
    var positionleft =  element.position().left;
    var positiontop = element.position().top;
    var left = element.position().left + config.offset;

    var arm = (left / config.pixelsPerInch) - (aircraft.offset / config.pixelsPerInch);
    var momentum = arm  * weight * config.kgtolbs / 1000;

    var mtow = aircraft.mtow;
    var bew = aircraft.bew;
    var fuelLBS = $("#aircraftFuelWeightLbs").innerText;
    var paylodAvail = mtow - bew - fuelLBS;

    var payload = 0;
    var sumamomentum2 = 0;


    var payloadArm = 0;

    $('#canvas').children('.skydiver').each(function() {

        var arm2 = ((($(this).position().left + config.offset )/ config.pixelsPerInch)) - (aircraft.offset / config.pixelsPerInch);


        if ( $(this).hasClass('boarded') ) {
            var weight2 = $(this).attr("data-weightkg");
            var weightbackup = weight2;
            var momentum2 = arm2 * weight2 * config.kgtolbs / 1000;
        } else {
            var weightbackup = $(this).attr("data-weightkg");
            var weight2 = 0;
            var momentum2 = 0;
            $(this).removeClass('boarded');
        }
        $(this).text(weightbackup + ' kg');

        payloadArm = payloadArm + arm2;
        sumamomentum2 = sumamomentum2 + momentum2;
        payload = payload + parseInt(weight2);

    } );

    var payloadlbs = payload * config.kgtolbs;
    var rampweight = payloadlbs + aircraft.bew + aircraft.fuellbs;


    var fuelMomentum = aircraft.fuellbs * aircraft.fuelArm / 1000;
    var rampmomentumEmpty = (aircraft.bew * aircraft.rampArm / 1000);

    rampmomentum = sumamomentum2 + rampmomentumEmpty + fuelMomentum;

    var offset = config.offset + aircraft.offset;
    var ramparm = rampmomentum * 1000 / rampweight;

    var weightLbs = weight * config.kgtolbs;


    $("#bew").text(aircraft.bew.toFixed(2));
    $("#bewArm").text(aircraft.rampArm.toFixed(2));
    var bewMomentum = aircraft.bew * aircraft.rampArm / 1000;
    $("#bewMomentum").text(bewMomentum.toFixed(2));

    $("#payload").text(payloadlbs.toFixed(2));
    $("#payloadArm").text((sumamomentum2*1000/payloadlbs).toFixed(2));
    $("#payloadMomentum").text(sumamomentum2.toFixed(2));

    var weightLessFuel = aircraft.bew + payloadlbs;
    var weightLessFuelArm = aircraft.rampArm + payloadArm;
    var weightLessFuelMomomentum = bewMomentum + sumamomentum2;
    $("#weightLessFuel").text(weightLessFuel.toFixed(2));
    $("#weightLessFuelArm").text((weightLessFuelMomomentum*1000/weightLessFuel).toFixed(2));
    $("#weightLessFuelMomentum").text(weightLessFuelMomomentum.toFixed(2));

    $("#fuelWeight").text(aircraft.fuellbs.toFixed(2));
    $("#fuelArm").text(aircraft.fuelArm.toFixed(2));
    var fuelMomentum = aircraft.fuellbs * aircraft.fuelArm / 1000;
    $("#fuelMomentum").text(fuelMomentum.toFixed(2));

    $("#rampWeight").text(rampweight.toFixed(2));
    $("#rampArm").text(ramparm.toFixed(2));
    $("#rampMomentum").text(rampmomentum.toFixed(2));

    var i=0;
    $('#manifest').empty();
    $('#canvas').children('.skydiver').each(function() {

        var left = $(this).position().left + config.offset;
        var arm = (left / config.pixelsPerInch) - (aircraft.offset / config.pixelsPerInch);
        var momentum = arm  * weight * config.kgtolbs / 1000;
        var tempWeightLbs = $(this).attr("data-weightkg") * config.kgtolbs;
        i++;
        $('#manifest').append('    <tr>\n' +
            '      <th scope="row">' + i + '</th>\n' +
            '      <td>Item #' + i + '</td>\n' +
            '      <td id="itemWeight' + 1 + '">' + tempWeightLbs + '</td>\n' +
            '      <td id="itemArm' + 1 + '">' + arm.toFixed(2) + '</td>\n' +
            '      <td id="itemMomentum' + 1 + '">' + momentum.toFixed(2) + '</td>\n' +
            '    </tr>');
    });

    $('#manifest').append('    <tr style="font-weight: bold">\n' +
        '      <th scope="row">' + i + '</th>\n' +
        '      <td>TOTAL PAYLOAD</td>\n' +
        '      <td id="payloadWeight' + 1 + '">' + payloadlbs.toFixed(2) + '</td>\n' +
        '      <td id="payloadArm' + 1 + '">' + (sumamomentum2*1000/payloadlbs).toFixed(2) + '</td>\n' +
        '      <td id="payloadMomentum' + 1 + '">' + sumamomentum2.toFixed(2) + '</td>\n' +
        '    </tr>');


   /* $('#results').append(
        '<br><br>____ AIRCRAFT ____' +
        '<br>Model: ' + aircraft.model +
        '<br>Reg: ' + aircraft.reg +
        '<br>Basic Empty Weight: ' + aircraft.bew.toFixed(2) +
        '<br>Fuel Weight: ' + aircraft.fuellbs.toFixed(2) +
        '<br>Payload (skydivers + pilot): ' + payloadlbs.toFixed(2) +
        '<br>Payload Available: ' + paylodAvail.toFixed(2) +
        '<br>________________' +
        '<br>Total Weight: ' + rampweight.toFixed(2) +
        '<br>Total Arm: ' + ramparm.toFixed(2) +
        '<br>Payload Momentum: ' + sumamomentum2.toFixed(2) +
        '<br>Total Momentum: ' + rampmomentum.toFixed(2) +
        '<br><br>____ LIMITS ____' +
        '<br>MTOW (Max Weight): ' + aircraft.mtow.toFixed(2) +
        '<br>Forward CoG limit: ' + aircraft.minArm +
        '<br>At CoG limit: ' + aircraft.maxArm +
        '<br><br>____ ACTIVE SKYDIVER ____' +
        '<br>Position Top: </vr>' + positiontop +
        '<br>Weight: ' + weightLbs.toFixed(2) +
        '<br>Left: </vr>' + positionleft +
        '<br>Left: </vr>' + left +
        '<br>Offset: ' + offset +
        '<br>Arm: ' + arm.toFixed(2) +
        '<br>Moment: ' + momentum.toFixed(2));*/

    var leftOffsetAfter = ramparm * config.pixelsPerInch + aircraft.offset;
    $('#cog').css( {"left": leftOffsetAfter});

    document.getElementById("preview-textfield").className = "preview-textfield";
    gauge.setTextField(document.getElementById("preview-textfield"));
    gauge.set(ramparm);

    document.getElementById("preview-textfield2").className = "preview-textfield";
    gauge2.setTextField(document.getElementById("preview-textfield2"));
    gauge2.set(rampweight);

    if ( ramparm > aircraft.maxArm || ramparm < aircraft.minArm || rampweight > aircraft.mtow) {
        $('.alert').css("background-color","red");
    }   else {
        $('.alert').css("background-color","#00bb00");
    }
}

$( "#btn1" ).click(function() {
    var skydiverweight = $('#weightDisplay').text();

    $('#canvas').append('<div data-toggle="modal" data-target="#exampleModal" data-weightkg="' + skydiverweight + '" class="result skydiver">' + skydiverweight+ ' kg</div>');
    $('.skydiver').draggable({
        revert: "invalid",
        start: function () {
            coordinates(this);
        },
        stop: function () {
            coordinates(this);
            },
        drag: function() {
            coordinates(this);
        }
    });

    $( "#canvas" ).droppable({

        drop: function( event, ui ) {
            ui.helper.data('dropped', true);
            $(ui.draggable)
                .addClass('boarded');
        }
    });

    $( "#boarding" ).droppable({

        drop: function( event, ui ) {
            $(ui.draggable)
                .removeClass('boarded');
        }
    });

    $( "#remove" ).droppable({

        drop: function( event, ui ) {
            $(ui.draggable)
                .remove();
        }
    });

});


var redArcMin = aircraft.minArm - ((aircraft.maxArm-aircraft.minArm)/100*34);
var redArcMax = aircraft.minArm;
var yellowArcMin = aircraft.minArm;
var yellowArcMax = aircraft.minArm + ((aircraft.maxArm-aircraft.minArm)/100*20);
var greenArcMin = aircraft.minArm + ((aircraft.maxArm-aircraft.minArm)/100*20);
var greenArcMax = aircraft.maxArm -((aircraft.maxArm-aircraft.minArm)/100*20);
var yellow2ArcMin = aircraft.maxArm -((aircraft.maxArm-aircraft.minArm)/100*20);
var yellow2ArcMax = aircraft.maxArm
var red2ArcMin = aircraft.maxArm
var red2ArcMax = aircraft.maxArm + ((aircraft.maxArm-aircraft.minArm)/100*34);

var g2ArcMin = aircraft.bew;
var g2ArcMax = aircraft.bew + (aircraft.mtow - aircraft.bew)/100*25;
var g3ArcMin = aircraft.bew + (aircraft.mtow - aircraft.bew)/100*25;
var g3ArcMax = aircraft.bew + (aircraft.mtow - aircraft.bew)/100*50;
var g4ArcMin = aircraft.bew + (aircraft.mtow - aircraft.bew)/100*50;
var g4ArcMax = aircraft.mtow - (aircraft.mtow - aircraft.bew)/100*25;
var y2ArcMin = aircraft.mtow - (aircraft.mtow - aircraft.bew)/100*25;
var y2ArcMax = aircraft.mtow;
var r2ArcMin = aircraft.mtow;
var r2ArcMax = aircraft.mtow + (aircraft.mtow - aircraft.bew)/100*25;

var opts = {
    angle: -0.07, /// The span of the gauge arc
    lineWidth: 0.44, // The line thickness
    pointer: {
        length: 0.9, // Relative to gauge radius
        strokeWidth: 0.045 // The thickness
    },
    staticZones: [
        {strokeStyle: "#F03E3E", min: redArcMin, max: redArcMax},
        {strokeStyle: "#FFDD00", min: yellowArcMin, max: yellowArcMax},
        {strokeStyle: "#99FF00", min: greenArcMin, max: greenArcMax},
        {strokeStyle: "#FFDD00", min: yellow2ArcMin , max:yellow2ArcMax},
        {strokeStyle: "#F03E3E", min: red2ArcMin, max: red2ArcMax}
    ],
    renderTicks: {
        divisions: 5,
        divWidth: 1.1,
        divLength: 0.7,
        divColor: "#333333",
        subLength: 0.5,
        subWidth: 0.6,
        subColor: "#666666"
    },
};

var opts2 = {
    angle: -0.07, /// The span of the gauge arc
    lineWidth: 0.44, // The line thickness
    pointer: {
        length: 0.9, // Relative to gauge radius
        strokeWidth: 0.035 // The thickness
    },
    staticZones: [
        {strokeStyle: "#33cc00", min: g2ArcMin, max: g2ArcMax},
        {strokeStyle: "#99FF00", min: g3ArcMin, max: g3ArcMax},
        {strokeStyle: "#CCFF33", min: g4ArcMin, max: g4ArcMax},
        {strokeStyle: "#FFdd00", min: y2ArcMin, max: y2ArcMax},
        {strokeStyle: "#F03E3E", min: r2ArcMin, max: r2ArcMax}
    ],
    renderTicks: {
        divisions: 4,
        divWidth: 1.1,
        divLength: 0.7,
        divColor: "#333333",
        subLength: 0.5,
        subWidth: 0.6,
        subColor: "#666666"
    },
};

document.getElementById("preview-textfield").className = "preview-textfield";
document.getElementById("preview-textfield2").className = "preview-textfield";

var fuelLBS = aircraft.fuellbs;
var fuelMomentum = fuelLBS * aircraft.fuelArm / 1000;

// Ramp momentum /1000
var rampmomentum = (aircraft.bew * aircraft.rampArm / 1000);
var rampmomentumwithFuel = rampmomentum + fuelMomentum;

// Ramp CoG (total momentum / total mass) in inches aft of datum
var weightwithFuel = aircraft.bew + fuelLBS;

var cog  = rampmomentumwithFuel * 1000 / weightwithFuel;

var target = document.getElementById('gauge'); // your canvas element
var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
gauge.setTextField(document.getElementById("preview-textfield"));
gauge.minValue = redArcMin;
gauge.maxValue = red2ArcMax; // set max gauge value
gauge.setMinValue(redArcMin);  // set min value

// INITIAL GAUGE SETTINGS (BEW + FUEL) (before adding any skydiver - welcome screen)
gauge.set(cog);


var leftOffset = cog * config.pixelsPerInch + aircraft.offset;
$('.vl').css( {"left": leftOffset});


var target2 = document.getElementById('gauge2'); // your canvas element
var gauge2 = new Gauge(target2).setOptions(opts2); // create sexy gauge!
gauge2.setTextField(document.getElementById("preview-textfield2"));
gauge2.minValue = g2ArcMin;
gauge2.maxValue = r2ArcMax; // set max gauge value
gauge2.setMinValue(g2ArcMin);  // set min value
gauge2.set(weightwithFuel);

var slider = document.getElementById("myRange");
var output = document.getElementById("weightDisplay");
output.innerHTML = slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
    output.innerHTML = this.value;
    var weightDisplayLbs = $("#weightDisplay").text() * config.kgtolbs;
    $("#weightDisplayLbs").text(' / ' + weightDisplayLbs.toFixed(2) + ' LBS');
}

var slider2 = document.getElementById("myRange2");
var output2 = document.getElementById("aircraftFuelWeightLbs");
output2.innerHTML = slider2.value; // Display the default slider value
slider2.oninput = function() {
    output2.innerHTML = this.value;
    aircraft.fuellbs = parseInt(this.value);
        coordinates(this);
}


var weightDisplayLbs = $("#weightDisplay").text() * config.kgtolbs;
$("#weightDisplayLbs").text(' / ' + weightDisplayLbs.toFixed(2) + ' LBS');

$('#buttonSave').click(function(event){

});


$('#exampleModal').on('shown.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var itemWeight = button.data('weightkg') * config.kgtolbs // Extract info from data-* attributes
    var itemArm = ( ( (button.position().left + config.offset ) / config.pixelsPerInch) + (aircraft.offset / config.pixelsPerInch) );
    console.log('itemArm:' + itemArm);
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('Change item parameters')
    modal.find('.modal-body input#formWeightLbs').val(itemWeight)
    modal.find('.modal-body input#formControlArm').val(itemArm);
    $('#buttonSave').click(function() {
        var weightFromForm = $('#formWeightLbs').val() / config.kgtolbs;
        var armFromForm = $('#formControlArm').val() * config.pixelsPerInch - ( config.offset * config.pixelsPerInch) - ( aircraft.offset * config.pixelsPerInch) ;
        button.attr("data-weightkg",weightFromForm.toFixed(1));
        button.css("left",armFromForm);
        coordinates(this);
    })
})




